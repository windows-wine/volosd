; Substitute for default tray icon routine in AHK
; Allows balloon tip in Win9x and separate click actions
; � Drugwash, April- 2011-June 2017, v1.2d
; file=icon index "|" file path
; Mouse buttons: LS LD RS RD MS MD XS XD (L=Left R=Right M=Middle X=XButton) S=Single click D=Double click
; Wheel movement: VU VD HU HD (V=Vertical H=Horizontal) U=Up D=Down
; par= Mouse/Wheel + M/L (Menu/Label action), use delimiter '|'
;#################################################
;################## public functions #####################
;#################################################
TrayIconSet(idx, hMain, file="", msg="", par="")
{
Static vers, cbSize
Global Ptr, AW, A_CharSize, PtrSz
if !(idx*hMain)
	return 0
ofi := A_FormatInteger
SetFormat, Integer, D
if (vers="")
	{
	if A_OSVersion in WIN_2000,WIN_XP,WIN_2003
		vers=3
	else vers := A_OSType="WIN32_WINDOWS" ? 0 : 4
;cbSize=88 (Win9x) 488 (2000-2003) 524 (Vista+)
;cbSize := 88+64*(vers>0)+336*(vers=3)+36*(vers=4)
	cbSize1 := 16+2*PtrSz+64*A_CharSize	; V1
	cbSize2 := cbSize1+16+320*A_CharSize	; V2 (shell v5)
	cbSize3 := cbSize2+16+PtrSz				; shell v6
	cbSize := vers=0 ? cbSize1 : vers=3 ? cbSize2 : cbSize3
	}
if file
	{
	StringSplit, i, file, |
	if A_IsCompiled
		{
		lib := DllCall("LoadLibrary" AW, "Str", A_ScriptFullPath, Ptr)
		hIcon := DllCall("LoadImage" AW
					, Ptr, lib
					, "Int", i1
					, "UInt", 1
					, "Int", 16
					, "Int", 16
					, "UInt", 0x2000
					, Ptr)
		hIconL := DllCall("LoadImage" AW
					, Ptr, lib
					, "Int", i1
					, "UInt", 1
					, "Int", 32
					, "Int", 32
					, "UInt", 0x2000
					, Ptr)
		DllCall("FreeLibrary", Ptr, lib)
		}
	else
		{
		hIcon := DllCall("LoadImage" AW
					, Ptr, 0
					, "Str", i2
					, "UInt", 1
					, "Int", 16
					, "Int", 16
					, "UInt", 0x2010
					, Ptr)
		hIconL := DllCall("LoadImage" AW
					, Ptr, 0
					, "Str", i2
					, "UInt", 1
					, "Int", 32
					, "Int", 32
					, "UInt", 0x2010
					, Ptr)
		if !(hIcon*hIconL)
			msgbox, Cannot load tray icon!
		}
	}
else hIcon=
j := Abs(idx)
flg := 1*(msg<>"") + 2*(file<>"")
VarSetCapacity(NID, cbSize, 0)				; NOTIFYICONDATA
NumPut(cbSize, NID, 0, "UInt")				; cbSize
NumPut(hMain, NID, 4, Ptr)					; hWnd
NumPut(j, NID, 4+PtrSz, "UInt")				; uID
NumPut(flg, NID, 8+PtrSz, "UInt")				; uFlags NIF_MESSAGE|NIF_ICON
NumPut(msg, NID, 12+PtrSz, "UInt")			; uCallbackMessage
NumPut(hIcon, NID, 16+PtrSz, Ptr)			; hIcon
if idx < 0
	{
	OnMessage(_TrayIconBuf(j, "MSG", ""), "")
	DllCall("shell32\Shell_NotifyIcon" AW, "UInt", 2, Ptr, &NID)	; NIM_DELETE
	_TrayIconBuf(j, "R", "")
	SetFormat, Integer, %ofi%
	return
	}
else if !_TrayIconBuf(j, "MSG", "") && hIcon
	{
	_TrayIconBuf(j, "A", "MSG" msg "|" par "|ICO" hIcon "|ICL" hIconL)
	if vers != 0
		{
		NumPut(vers, NID, 24+2*PtrSz+320*A_CharSize, "UInt")	; uVersion
		DllCall("shell32\Shell_NotifyIcon" AW, "UInt", 4, Ptr, &NID)	; NIM_SETVERSION
		NumPut(7, NID, 24+2*PtrSz+320*A_CharSize, "UInt")		; uTimeout
		NumPut(flg | 0x10, NID, 8+PtrSz, "UInt")	; uFlags NIF_MESSAGE|NIF_ICON|NIF_INFO
		}
	DllCall("shell32\Shell_NotifyIcon" AW, "UInt", 0, Ptr, &NID)	; NIM_ADD
	}
else
	DllCall("shell32\Shell_NotifyIcon" AW, "UInt", 1, Ptr, &NID)	; NIM_MODIFY
if hIcon
	DllCall("DestroyIcon", Ptr, hIcon)
OnMessage(msg, "_TrayIconAct")
SetFormat, Integer, %ofi%
}
;================================================================
TrayIconTip(idx, hMain, txt="", ttl="", tout="", file="", args="")
;================================================================
{
Static vers, cbsize
Global Ptr, AW, A_CharSize, PtrSz, debug
if (vers="")
	{
	if A_OSVersion in WIN_2000,WIN_XP,WIN_2003
		vers=3
	else vers := A_OSType="WIN32_WINDOWS" ? 0 : 4
; do the right math here!
;cbSize := 88+64*(vers>0)+336*(vers=3)+36*(vers=4)
	cbSize1 := 16+2*PtrSz+64*A_CharSize	; V1
	cbSize2 := cbSize1+16+320*A_CharSize	; V2 (shell v5)
	cbSize3 := cbSize2+16+PtrSz				; shell v6
	cbSize := vers=0 ? cbSize1 : vers=3 ? cbSize2 : cbSize3
	}
if !(idx*hMain)
	return 0
j := Abs(idx)
flg := (vers && ttl) ? 0x10 : 0x4
VarSetCapacity(NID, cbSize, 0)	; NOTIFYICONDATA
NumPut(cbSize, NID, 0, "UInt")	; cbSize
NumPut(hMain, NID, 4, UPtr)		; hWnd
NumPut(j, NID, 4+PtrSz, "UInt")	; uID
NumPut(flg, NID, 8+PtrSz, "UInt")	; NIF_TIP 0x4/NIF_INFO 0x10
if idx > 0
	{
	if vers=0
		DllCall("lstrcpyn" AW, Ptr, &NID+16+2*PtrSz, Ptr, &txt, "UInt", 63)
	else
		{
		DllCall("lstrcpyn" AW, Ptr, &NID+16+2*PtrSz, Ptr, &txt, "UInt", 63)
		if i := InStr(ttl, "|")
			{
			StringLeft, icon, ttl, i-1
			StringTrimLeft, ttl, ttl, i
			}
		DllCall("lstrcpyn" AW, Ptr, &NID+24+2*PtrSz+64*A_CharSize, Ptr, &txt, "UInt", 255)
		if ttl
			DllCall("lstrcpyn" AW, Ptr, &NID+28+2*PtrSz+320*A_CharSize, Ptr, &ttl, "UInt", 63)
		if tout
			NumPut(tout, NID, 24+2*PtrSz+320*A_CharSize, "UInt")	; uTimeout
		if icon
			{
			NumPut(icon, NID, 28+2*PtrSz+384*A_CharSize, "UInt")	; dwInfoFlags
			ico :=  (icon & 0x20) ? "ICL" : "ICO"	; NIIF_LARGE_ICON
			NumPut(i := _TrayIconBuf(idx, ico), NID, 16+PtrSz, Ptr)	 ; hIcon
			if (icon & 0x4)	; NIIF_USER
				{
				if vers=4
					NumPut(i, NID, 48+2*PtrSz+384*A_CharSize, Ptr) ; hBalloonIcon
				}
			}
		}
	}
r := DllCall("shell32\Shell_NotifyIcon" AW, "UInt", 1, Ptr, &NID)	; NIM_MODIFY
;if debug
OutputDebug,
	(LTrim
	vers=%vers%
	idx=%idx%
	cbSize=%cbSize%
	flg=%flg%
	hMain=%hMain%
	icon=%icon%
	ico=%ico%
	i=%i%
	timeout=%col%
	r=%r%
	txt=%txt%
	ttl=%ttl%
	)
return r
}
;#################################################
;################# private functions #####################
;#################################################
;================================================================
_TrayIconAct(wP, lP, msg, hwnd)
;================================================================
{
Static
if (vers="")
	{
	if A_OSVersion in WIN_2000,WIN_XP,WIN_2003
		vers=3
	else vers :=  A_OSType="WIN32_WINDOWS" ? 0 : 4
	}
if msg=0
	onclick := wP	; label to be launched when user clicks the tray balloon
n := lP & 0xFFFF	; notification message (see usage of NOTIFYICON_VERSION_4)
w := (lP >> 16) & 0xFFFF	; icon ID (currently not used but necessary when we add more than one tray icon per app)
i := vers=4 ? w : wP
c := DllCall("GetDoubleClickTime", "UInt")
	if n=0x201		; WM_LBUTTONDOWN
		{
		b=LS
		SetTimer, sc, -%c% 
		}
	else if n=0x204	; WM_RBUTTONDOWN
		{
		b=RS
		SetTimer, sc, -%c% 
		}
	else if n=0x207	; WM_MBUTTONDOWN
		{
		b=MS
		SetTimer, sc, -%c% 
		}
	else if n=0x20B	; WM_XBUTTONDOWN
		{
		b=XS
		SetTimer, sc, -%c% 
		}
	else if n=0x203	; WM_LBUTTONDBLCLK
		{
		b=LD
		SetTimer, sc, -1
		}
	else if n=0x206	; WM_RBUTTONDBLCLK
		{
		b=RD
		SetTimer, sc, -1
		}
	else if n=0x209	; WM_MBUTTONDBLCLK
		{
		b=MD
		SetTimer, sc, -1
		}
	else if n=0x20D	; WM_XBUTTONDBLCLK
		{
		b=XD
		SetTimer, sc, -1
		}
	else if n=0x402 OR n=0x403	; NIN_SELECT NIN_KEYSELECT
		Menu, Tray, Show
	else if n=0x404	; NIN_BALLOONTIMEOUT
		TrayIconTip(i, hwnd)
	else if n=0x405 && onclick	; NIN_BALLOONUSERCLICK
		SetTimer, %onclick%, -1
/*
; check for balloon NIN_* 0x402 0x404, WM_MOUSEWHEEL 0x20A, WM_MOUSEHWHEEL 0x20E
	else if n != 0x200
		{
		ofi := A_FormatInteger
		SetFormat, Integer, H
		n+=0
		FileAppend, msg %n%`n, Other Tray activity.txt
		SetFormat, Integer, %ofi%
		}
*/
return

sc:
_TrayIconBuf(i, "L", b)
return
}
;================================================================
_TrayIconBuf(idx, op="", par="")
;================================================================
{
Static
Global Ptr
com := "LSM,LSL,LDM,LDL,RSM,RSL,RDM,RDL,MSM,MSL,MDM,MDL,XSM,XSL,XDM,XDL,VUM,VUL,VDM,VDL
		,HUM,HUL,HDM,HDL,HWD,MSG,ICO"
ofi := A_FormatInteger
SetFormat, Integer, D
idx+=0
SetFormat, Integer, %ofi%
if op=A	; add
	{
	Loop, Parse, par, |
		{
		StringLeft, pre, A_LoopField, 3
		StringTrimLeft, data, A_LoopField, 3
		%pre%%idx% := data
		}
	}
else if op=R	; remove
	{
	DllCall("DestroyIcon", Ptr, ICO%idx%)
	Loop, Parse, com, CSV
		%A_LoopField%%idx% := ""
	}
else if op=L	; launch
	{
	menu := %par%M%idx%
	label := %par%L%idx%
	if (act := menu)
		Menu, %act%, Show
	else if IsLabel(act := label)
		SetTimer, %act%, -1
	}
else if op in %com%	; retrieve
	return i := %op%%idx%
}
