; detect attached monitors and videocards
;version = 2.0
;released = June 17, 2017

/*
MSDN reference:

BOOL EnumDisplayDevices(
  LPCTSTR lpDevice,				// device name (in) pointer
  DWORD iDevNum,					// display device (in)
  PDISPLAY_DEVICE lpDisplayDevice,	// device information (out) struct
  DWORD dwFlags					// reserved (in)
);

typedef struct _DISPLAY_DEVICE {
  DWORD cb;
  TCHAR DeviceName[32];
  TCHAR DeviceString[128];
  DWORD StateFlags;
  TCHAR DeviceID[128];
  TCHAR DeviceKey[128];
} DISPLAY_DEVICE, *PDISPLAY_DEVICE;
***********************************
DISPLAY_DEVICE_ATTACHED_TO_DESKTOP = 0x1
DISPLAY_DEVICE_MULTI_DRIVER = 0x2
DISPLAY_DEVICE_PRIMARY_DEVICE = 0x4
DISPLAY_DEVICE_MIRRORING_DRIVER = 0x8
DISPLAY_DEVICE_VGA_COMPATIBLE = 0x10
DISPLAY_DEVICE_REMOVABLE = 0x20
DISPLAY_DEVICE_MODESPRUNED = 0x8000000
DISPLAY_DEVICE_REMOTE = 0x4000000
DISPLAY_DEVICE_DISCONNECT = 0x2000000

DISPLAY_DEVICE_ACTIVE = 0x1
DISPLAY_DEVICE_ATTACHED = 0x2

CCHDEVICENAME = 32
CCHFORMNAME = 32

ENUM_CURRENT_SETTINGS = -1
ENUM_REGISTRY_SETTINGS = -2

User32.dll: MonitorFromWindow
MONITOR_DEFAULTTONULL = 0
MONITOR_DEFAULTTOPRIMARY = 1
MONITOR_DEFAULTTONEAREST = 2

EDD_GET_DEVICE_INTERFACE_NAME = 0x00000001
************************************
updates()
detmon()
MsgBox, Active monitors: %MonAct%`nPrimary monitor: %MonPrim%`nMonitor1: %MonNam1%`nMonitor2: %MonNam2%`nMonitor3: %MonNam3%`nMonitor4: %MonNam4%
return
*/

detmon()
{
Global MonAct, MonPrim, MonNam1, MonNam2, MonNam3, MonNam4, flg1, flg2, flg3, flg4, Ptr, A_CharSize, AW
;struct DISPLAY_DEVICE dd
;struct DISPLAY_DEVICE ddMon
DISPLAY_DEVICE_ACTIVE = 0x00000001				; ask MSDN
DISPLAY_DEVICE_PRIMARY_DEVICE = 0x00000004		; ask MSDN
DISPLAY_DEVICE_MIRRORING_DRIVER = 0x00000008	; ask MSDN
;DISPLAY_DEVICE_VGA_COMPATIBLE = 0x10
;DISPLAY_DEVICE_REMOVABLE = 0x20
;DISPLAY_DEVICE_MODESPRUNED = 0x8000000			; only these are mentioned at MSDN for the structure
sz := 8+416*A_CharSize
VarSetCapacity(dd, sz, 0)		; allocate space for the dd structure
VarSetCapacity(ddMon, sz, 0)	; allocate space for the ddMon structure
VarSetCapacity(dd_DeviceName, 32*A_CharSize, 0)
VarSetCapacity(dd_DeviceString, 128*A_CharSize, 0)
VarSetCapacity(ddMon_DeviceName, 32*A_CharSize, 0)
VarSetCapacity(ddMon_DeviceString, 128*A_CharSize, 0)

dev = 0					; device index
id = 1					; monitor number, as used by Display Properties > Settings
Loop
	{
	NumPut(sz, dd, 0, "UInt")
	dd_DeviceName =
	dd_DeviceString =
	if ! DllCall("EnumDisplayDevices" AW, Ptr, 0, "UInt", dev, Ptr, &dd, "UInt", 0, "UInt")
		break
	DllCall("lstrcpyn" AW, Ptr, &dd_DeviceName, Ptr, &dd+4, "UInt", 32)
	DllCall("lstrcpyn" AW, Ptr, &dd_DeviceString, Ptr, &dd+4+32*A_CharSize, "UInt", 128)
	VarSetCapacity(dd_DeviceName, -1), VarSetCapacity(dd_DeviceString, -1)
	dd_StateFlags := NumGet(dd, 4+160*A_CharSize, "UInt")
	flg%id% := dd_StateFlags
	if ! (dd_StateFlags & DISPLAY_DEVICE_MIRRORING_DRIVER)	; ignore virtual mirror displays
		{
		devMon = 0
		Loop
			{
			NumPut(sz, ddMon, 0, "UInt")
			ddMon_DeviceName =
			ddMon_DeviceString =
			if ! DllCall("EnumDisplayDevices" AW, "Str", dd_DeviceName, "UInt", devMon, Ptr, &ddMon, "UInt", 0, "UInt")
				break
			DllCall("lstrcpyn" AW, Ptr, &ddMon_DeviceName, Ptr, &dd+4, "UInt", 32)
			DllCall("lstrcpyn" AW, Ptr, &ddMon_DeviceString, Ptr, &ddMon+4+32*A_CharSize, "UInt", 128)
			VarSetCapacity(ddMon_DeviceName, -1), VarSetCapacity(ddMon_DeviceString, -1)
			ddMon_StateFlags := NumGet(ddMon, 4+160*A_CharSize, "UInt")
			if (ddMon_StateFlags & DISPLAY_DEVICE_ACTIVE)
				break
			devMon++
			}
		if ! ddMon_DeviceString
			{
			NumPut(sz, ddMon, 0, "UInt")
			ddMon_DeviceName =
			ddMon_DeviceString =
			DllCall("EnumDisplayDevices" AW, "Str", dd_DeviceName, "UInt", 0, Ptr, &ddMon, "UInt", 0, "UInt")
			DllCall("lstrcpyn" AW, Ptr, &ddMon_DeviceName, Ptr, &dd+4, "UInt", 32)
			DllCall("lstrcpyn" AW, Ptr, &ddMon_DeviceString, Ptr, &ddMon+4+32*A_CharSize, "UInt", 128)
			VarSetCapacity(ddMon_DeviceName, -1), VarSetCapacity(ddMon_DeviceString, -1)
			ddMon_StateFlags := NumGet(ddMon, 4+160*A_CharSize, "UInt")
			if ! ddMon_DeviceString
				ddMon_DeviceString := "Default Monitor"
			}
		MonNam%id% := ddMon_DeviceString . "`n on " . dd_DeviceString
		if (dd_StateFlags & DISPLAY_DEVICE_PRIMARY_DEVICE)
			MonPrim := id
		id++
		}
	dev++	
	}
MonAct := dev
return MonAct, MonPrim, MonNam1, MonNam2, MonNam3, MonNam4, flg1, flg2, flg3, flg4
}
